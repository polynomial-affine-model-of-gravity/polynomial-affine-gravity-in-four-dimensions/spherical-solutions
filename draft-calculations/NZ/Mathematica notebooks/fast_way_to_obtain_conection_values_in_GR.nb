(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     23728,        619]
NotebookOptionsPosition[     21269,        574]
NotebookOutlinePosition[     21710,        591]
CellTagsIndexPosition[     21667,        588]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"ClearAll", "[", "\"\<Global`*\>\"", "]"}]], "Input",
 CellLabel->
  "In[135]:=",ExpressionUUID->"ce3c1d06-7b83-4c33-be92-1d40b9acc002"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"T", "[", 
    RowBox[{"t", ",", "r"}], "]"}], "=", 
   RowBox[{"f", "[", 
    RowBox[{"t", ",", "r"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"R", "[", 
    RowBox[{"t", ",", "r"}], "]"}], "=", 
   RowBox[{"T", "[", 
    RowBox[{"t", ",", "r"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"S", "[", 
    RowBox[{"t", ",", "r"}], "]"}], "=", 
   SuperscriptBox["r", "2"]}], ";"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{
  3.900066385935793*^9, 3.900066433138528*^9, {3.9000680610352187`*^9, 
   3.9000680764252543`*^9}, {3.9000681668667965`*^9, 
   3.9000681695155926`*^9}, {3.9000742731750035`*^9, 3.900074302777731*^9}, {
   3.9000749766591296`*^9, 3.9000750005008125`*^9}, {3.9000750428018875`*^9, 
   3.900075049281488*^9}, 3.9000759946494393`*^9, {3.90231936099638*^9, 
   3.9023193619623113`*^9}, {3.90482044749749*^9, 3.9048204551167817`*^9}, {
   3.9048208109929676`*^9, 3.904820819458256*^9}, 3.90489199215724*^9, 
   3.904892041518804*^9, {3.905331541487235*^9, 3.905331573660013*^9}, {
   3.905333122160981*^9, 3.9053331392363844`*^9}, {3.90553760437755*^9, 
   3.9055376594040833`*^9}, {3.9055378365106335`*^9, 
   3.9055378449752283`*^9}, {3.906459148810727*^9, 3.906459161636673*^9}, {
   3.9064592712608633`*^9, 3.90645928554976*^9}, 3.907320509218394*^9, {
   3.9073205522911816`*^9, 3.907320563909423*^9}, {3.908974673989152*^9, 
   3.908974674322804*^9}},ExpressionUUID->"8c5b4fa2-0655-434a-a60a-\
06abbc1b1751"],

Cell[BoxData[
 TemplateBox[{
  "Set", "write", 
   "\"Tag \\!\\(\\*RowBox[{\\\"D\\\"}]\\) in \\!\\(\\*RowBox[{SubscriptBox[\\\
\"\[PartialD]\\\", RowBox[{\\\"r\\\"}]], \\\"t\\\"}]\\) is Protected.\"", 2, 
   154, 18, 27088271591439445626, "Local"},
  "MessageTemplate"]], "Message", "MSG",
 CellChangeTimes->{{3.9073205606171064`*^9, 3.9073205643582373`*^9}},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[152]:=",ExpressionUUID->"b670c8f0-7b14-42be-8759-3b538d255d15"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.905537622124815*^9, 
  3.9055376256499853`*^9}},ExpressionUUID->"28fb65cf-6c25-4aed-9fe2-\
a5b72ba33956"],

Cell[CellGroupData[{

Cell["metrica ", "Subsection",
 CellChangeTimes->{{3.900066121308103*^9, 3.9000661243710804`*^9}, {
   3.900075053008146*^9, 3.900075054064286*^9}, 
   3.904820824068987*^9},ExpressionUUID->"979698ff-6316-429c-a33e-\
ef2545401f05"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"d", "=", "4"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"gg", "=", 
   RowBox[{"(", "\[NoBreak]", GridBox[{
      {
       RowBox[{"-", 
        RowBox[{"T", "[", "r", "]"}]}], "0", "0", "0"},
      {"0", 
       FractionBox["1", 
        RowBox[{"R", "[", "r", "]"}]], "0", "0"},
      {"0", "0", 
       RowBox[{"S", "[", "r", "]"}], "0"},
      {"0", "0", "0", 
       RowBox[{
        RowBox[{"S", "[", "r", "]"}], " ", 
        SuperscriptBox[
         RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}
     }], "\[NoBreak]", ")"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"i", "=", "0"}], ",", 
    RowBox[{"i", "<", "d"}], ",", 
    RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"For", "[", 
     RowBox[{
      RowBox[{"j", "=", "0"}], ",", 
      RowBox[{"j", "<", "d"}], ",", 
      RowBox[{"j", "++"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{"gdown", "[", 
         RowBox[{"i", ",", "j"}], "]"}], "=", 
        RowBox[{
         RowBox[{"gg", "\[LeftDoubleBracket]", 
          RowBox[{"i", "+", "1"}], "\[RightDoubleBracket]"}], 
         "\[LeftDoubleBracket]", 
         RowBox[{"j", "+", "1"}], "\[RightDoubleBracket]"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"gup", "[", 
         RowBox[{"i", ",", "j"}], "]"}], "=", 
        RowBox[{
         RowBox[{
          RowBox[{"Inverse", "[", "gg", "]"}], "\[LeftDoubleBracket]", 
          RowBox[{"i", "+", "1"}], "\[RightDoubleBracket]"}], 
         "\[LeftDoubleBracket]", 
         RowBox[{"j", "+", "1"}], "\[RightDoubleBracket]"}]}], ";"}]}], 
     "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"regla", "=", 
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      RowBox[{"x", "[", "0", "]"}], "\[Rule]", "t"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"x", "[", "1", "]"}], "\[Rule]", "r"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"x", "[", "2", "]"}], "\[Rule]", "\[Theta]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"x", "[", "3", "]"}], "\[Rule]", "\[Phi]"}]}], 
    "\[IndentingNewLine]", "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"ClearAll", "[", 
  RowBox[{"\[Mu]", ",", "\[Nu]", ",", "\[Rho]", ",", "\[Sigma]"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"For", "[", 
  RowBox[{
   RowBox[{"\[Mu]", "=", "0"}], ",", 
   RowBox[{"\[Mu]", "<", "d"}], ",", 
   RowBox[{"\[Mu]", "++"}], ",", "\[IndentingNewLine]", 
   RowBox[{"For", "[", 
    RowBox[{
     RowBox[{"\[Nu]", "=", "0"}], ",", 
     RowBox[{"\[Nu]", "<", "d"}], ",", 
     RowBox[{"\[Nu]", "++"}], ",", "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"\[Rho]", "=", "0"}], ",", 
       RowBox[{"\[Rho]", "<", "d"}], ",", 
       RowBox[{"\[Rho]", "++"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{"\[CapitalGamma]", "[", 
          RowBox[{"\[Mu]", ",", "\[Nu]", ",", "\[Rho]"}], "]"}], "=", 
         RowBox[{
          FractionBox["1", "2"], " ", 
          RowBox[{"Sum", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"gup", "[", 
              RowBox[{"\[Rho]", ",", "\[Sigma]"}], "]"}], " ", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{"D", "[", 
                RowBox[{
                 RowBox[{"gdown", "[", 
                  RowBox[{"\[Nu]", ",", "\[Sigma]"}], "]"}], ",", 
                 RowBox[{
                  RowBox[{"x", "[", "\[Mu]", "]"}], "/.", "regla"}]}], "]"}], 
               "+", 
               RowBox[{"D", "[", 
                RowBox[{
                 RowBox[{"gdown", "[", 
                  RowBox[{"\[Sigma]", ",", "\[Mu]"}], "]"}], ",", 
                 RowBox[{
                  RowBox[{"x", "[", "\[Nu]", "]"}], "/.", "regla"}]}], "]"}], 
               "-", 
               RowBox[{"D", "[", 
                RowBox[{
                 RowBox[{"gdown", "[", 
                  RowBox[{"\[Mu]", ",", "\[Nu]"}], "]"}], ",", 
                 RowBox[{
                  RowBox[{"x", "[", "\[Sigma]", "]"}], "/.", "regla"}]}], 
                "]"}]}], ")"}]}], ",", 
            RowBox[{"{", 
             RowBox[{"\[Sigma]", ",", "0", ",", 
              RowBox[{"d", "-", "1"}]}], "}"}]}], "]"}]}]}], ";"}]}], 
      "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", "]"}]}], 
  "\[IndentingNewLine]", "]"}]}], "Input",
 CellChangeTimes->{{3.9000652936698847`*^9, 3.9000652945138135`*^9}, {
   3.900065339470895*^9, 3.9000654112763815`*^9}, {3.900065724229305*^9, 
   3.900065745684209*^9}, {3.900065799410194*^9, 3.9000658105659647`*^9}, {
   3.9000660900322824`*^9, 3.9000661142882752`*^9}, {3.9000666319146957`*^9, 
   3.9000666328482*^9}, {3.9053332159555025`*^9, 3.905333218478778*^9}, {
   3.90553764506219*^9, 3.9055376648364773`*^9}, {3.9064591690286474`*^9, 
   3.906459180083082*^9}, 3.9073193745990562`*^9, {3.9073204658471837`*^9, 
   3.907320479138671*^9}, {3.907320599398841*^9, 3.9073206059987907`*^9}, {
   3.907320668174759*^9, 3.9073206794707584`*^9}},
 CellLabel->"In[37]:=",ExpressionUUID->"f57d78e8-cc3c-48fa-8106-08de4af8ec0c"]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[{
 StyleBox["S\[IAcute]mbolos",
  IgnoreSpellCheck->True],
 " de Christoffel \[Rule] ",
 Cell[BoxData[
  FormBox[
   SubsuperscriptBox["\[CapitalGamma]", 
    RowBox[{"\[Mu]", " ", "\[Nu]"}], "\[Rho]"], TraditionalForm]], "None",
  ExpressionUUID->"c172ff98-3261-4e66-9434-35e6e438a816"],
 " = \[CapitalGamma] [\[Mu],\[Nu],\[Rho]]"
}], "Subsection",
 CellChangeTimes->{
  3.9000660629175615`*^9},ExpressionUUID->"4f4f47be-f94b-45f8-b339-\
e2ffd8b36c7f"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"For", "[", 
  RowBox[{
   RowBox[{"\[Mu]", "=", "0"}], ",", 
   RowBox[{"\[Mu]", "<", "d"}], ",", 
   RowBox[{"\[Mu]", "++"}], ",", "\[IndentingNewLine]", 
   RowBox[{"For", "[", 
    RowBox[{
     RowBox[{"\[Nu]", "=", "0"}], ",", 
     RowBox[{"\[Nu]", "<", "d"}], ",", 
     RowBox[{"\[Nu]", "++"}], ",", "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"\[Rho]", "=", "0"}], ",", 
       RowBox[{"\[Rho]", "<", "d"}], ",", 
       RowBox[{"\[Rho]", "++"}], ",", "\[IndentingNewLine]", 
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{"TrueQ", "[", 
          RowBox[{
           RowBox[{"\[CapitalGamma]", "[", 
            RowBox[{"\[Mu]", ",", "\[Nu]", ",", "\[Rho]"}], "]"}], "\[Equal]",
            "0"}], "]"}], ",", ",", 
         RowBox[{"Print", "[", 
          RowBox[{
          "\[Mu]", ",", "\[Nu]", ",", "\[Rho]", ",", "\"\<  \>\"", ",", 
           RowBox[{"Simplify", "[", 
            RowBox[{"\[CapitalGamma]", "[", 
             RowBox[{"\[Mu]", ",", "\[Nu]", ",", "\[Rho]"}], "]"}], "]"}]}], 
          "]"}]}], "]"}]}], "\[IndentingNewLine]", "]"}]}], 
    "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", "]"}]], "Input",
 CellChangeTimes->{{3.900065292041394*^9, 3.900065320896823*^9}, {
  3.9000654390413404`*^9, 3.9000654451100693`*^9}, {3.9000666201109495`*^9, 
  3.9000666467164493`*^9}},
 CellLabel->"In[43]:=",ExpressionUUID->"deb687bb-1c93-416f-88e0-73d7d39aba86"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "0", "\[InvisibleSpace]", "0", "\[InvisibleSpace]", "1", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"R", "[", "r", "]"}], " ", 
    RowBox[{
     SuperscriptBox["T", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}]}]}],
  SequenceForm[
  0, 0, 1, "  ", Rational[1, 2] $CellContext`R[$CellContext`r] 
   Derivative[1][$CellContext`T][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678589985*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"bb5e93d6-471f-457c-9761-8d2c46c365e0"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "0", "\[InvisibleSpace]", "1", "\[InvisibleSpace]", "0", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{
     SuperscriptBox["T", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}], 
    RowBox[{"2", " ", 
     RowBox[{"T", "[", "r", "]"}]}]]}],
  SequenceForm[
  0, 1, 0, "  ", Rational[1, 2] $CellContext`T[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`T][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678605621*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"13824424-fc4a-476a-b539-262092524089"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "0", "\[InvisibleSpace]", "0", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{
     SuperscriptBox["T", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}], 
    RowBox[{"2", " ", 
     RowBox[{"T", "[", "r", "]"}]}]]}],
  SequenceForm[
  1, 0, 0, "  ", Rational[1, 2] $CellContext`T[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`T][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.9089746786116276`*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"5441355a-c167-437a-a17c-cba5b63e37cf"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "1", "\[InvisibleSpace]", "1", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{"-", 
    FractionBox[
     RowBox[{
      SuperscriptBox["R", "\[Prime]",
       MultilineFunction->None], "[", "r", "]"}], 
     RowBox[{"2", " ", 
      RowBox[{"R", "[", "r", "]"}]}]]}]}],
  SequenceForm[
  1, 1, 1, "  ", Rational[-1, 2] $CellContext`R[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`R][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678621636*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"cc198d08-8e47-4b1d-bbae-1f86ac35b93a"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "2", "\[InvisibleSpace]", "2", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{
     SuperscriptBox["S", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}], 
    RowBox[{"2", " ", 
     RowBox[{"S", "[", "r", "]"}]}]]}],
  SequenceForm[
  1, 2, 2, "  ", Rational[1, 2] $CellContext`S[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`S][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678621636*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"5656aa26-54d2-4702-b40d-fa67c3b9fed2"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "1", "\[InvisibleSpace]", "3", "\[InvisibleSpace]", "3", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{
     SuperscriptBox["S", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}], 
    RowBox[{"2", " ", 
     RowBox[{"S", "[", "r", "]"}]}]]}],
  SequenceForm[
  1, 3, 3, "  ", Rational[1, 2] $CellContext`S[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`S][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678621636*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"831e4bfe-f50f-40d9-8ac1-a1ec173d38fa"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "2", "\[InvisibleSpace]", "1", "\[InvisibleSpace]", "2", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{
     SuperscriptBox["S", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}], 
    RowBox[{"2", " ", 
     RowBox[{"S", "[", "r", "]"}]}]]}],
  SequenceForm[
  2, 1, 2, "  ", Rational[1, 2] $CellContext`S[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`S][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.9089746786372757`*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"7b9ffd51-4b73-4a42-a1b8-3271872a9aed"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "2", "\[InvisibleSpace]", "2", "\[InvisibleSpace]", "1", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     FractionBox["1", "2"]}], " ", 
    RowBox[{"R", "[", "r", "]"}], " ", 
    RowBox[{
     SuperscriptBox["S", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}]}]}],
  SequenceForm[
  2, 2, 1, "  ", Rational[-1, 2] $CellContext`R[$CellContext`r] 
   Derivative[1][$CellContext`S][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.9089746786452937`*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"2f7b42c7-8c8d-44f2-aa4d-6980bbf187ba"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "2", "\[InvisibleSpace]", "3", "\[InvisibleSpace]", "3", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{"Cot", "[", "\[Theta]", "]"}]}],
  SequenceForm[2, 3, 3, "  ", 
   Cot[$CellContext`\[Theta]]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.9089746786533365`*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"611811b1-a177-4f2e-b432-7c17a43fe6bc"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "3", "\[InvisibleSpace]", "1", "\[InvisibleSpace]", "3", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   FractionBox[
    RowBox[{
     SuperscriptBox["S", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}], 
    RowBox[{"2", " ", 
     RowBox[{"S", "[", "r", "]"}]}]]}],
  SequenceForm[
  3, 1, 3, "  ", Rational[1, 2] $CellContext`S[$CellContext`r]^(-1) 
   Derivative[1][$CellContext`S][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.9089746786533365`*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"c4311994-5562-44d6-9041-2d99199c7dc2"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "3", "\[InvisibleSpace]", "2", "\[InvisibleSpace]", "3", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{"Cot", "[", "\[Theta]", "]"}]}],
  SequenceForm[3, 2, 3, "  ", 
   Cot[$CellContext`\[Theta]]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.9089746786533365`*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"e71c5d03-8774-46fc-a0cd-e150a2d8361d"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "3", "\[InvisibleSpace]", "3", "\[InvisibleSpace]", "1", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     FractionBox["1", "2"]}], " ", 
    RowBox[{"R", "[", "r", "]"}], " ", 
    SuperscriptBox[
     RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"], " ", 
    RowBox[{
     SuperscriptBox["S", "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}]}]}],
  SequenceForm[
  3, 3, 1, "  ", Rational[-1, 2] $CellContext`R[$CellContext`r] 
   Sin[$CellContext`\[Theta]]^2 Derivative[1][$CellContext`S][$CellContext`r]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678669447*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"eecae791-2287-41e7-be4b-6dade7e0816d"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
  "3", "\[InvisibleSpace]", "3", "\[InvisibleSpace]", "2", 
   "\[InvisibleSpace]", "\<\"  \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     RowBox[{"Cos", "[", "\[Theta]", "]"}]}], " ", 
    RowBox[{"Sin", "[", "\[Theta]", "]"}]}]}],
  SequenceForm[
  3, 3, 2, "  ", -Cos[$CellContext`\[Theta]] Sin[$CellContext`\[Theta]]],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.9055380095304317`*^9, 3.9064592175674133`*^9, {3.906459277945771*^9, 
   3.906459291844402*^9}, 3.9073205401508603`*^9, 3.9073206114522753`*^9, 
   3.907320686673483*^9, 3.908974678669447*^9},
 CellLabel->
  "Durante la evaluaci\[OAcute]n de \
In[43]:=",ExpressionUUID->"88c96198-8c53-4567-acba-5903764eab6b"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1152., 579.6},
WindowMargins->{{
  Automatic, -5.399999999999864}, {-5.399999999999977, Automatic}},
FrontEndVersion->"13.2 para Microsoft Windows (64-bit) (January 30, 2023)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"c9fe2588-3d6b-4cc4-af2e-25e845b4d8a8"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 159, 3, 43, "Input",ExpressionUUID->"ce3c1d06-7b83-4c33-be92-1d40b9acc002"],
Cell[CellGroupData[{
Cell[742, 27, 1554, 33, 86, "Input",ExpressionUUID->"8c5b4fa2-0655-434a-a60a-06abbc1b1751"],
Cell[2299, 62, 477, 10, 28, "Message",ExpressionUUID->"b670c8f0-7b14-42be-8759-3b538d255d15"]
}, Open  ]],
Cell[2791, 75, 154, 3, 28, "Input",ExpressionUUID->"28fb65cf-6c25-4aed-9fe2-a5b72ba33956"],
Cell[CellGroupData[{
Cell[2970, 82, 231, 4, 54, "Subsection",ExpressionUUID->"979698ff-6316-429c-a33e-ef2545401f05"],
Cell[3204, 88, 5318, 134, 722, "Input",ExpressionUUID->"f57d78e8-cc3c-48fa-8106-08de4af8ec0c"]
}, Closed]],
Cell[CellGroupData[{
Cell[8559, 227, 467, 13, 41, "Subsection",ExpressionUUID->"4f4f47be-f94b-45f8-b339-e2ffd8b36c7f"],
Cell[CellGroupData[{
Cell[9051, 244, 1471, 34, 208, "Input",ExpressionUUID->"deb687bb-1c93-416f-88e0-73d7d39aba86"],
Cell[CellGroupData[{
Cell[10547, 282, 847, 21, 38, "Print",ExpressionUUID->"bb5e93d6-471f-457c-9761-8d2c46c365e0"],
Cell[11397, 305, 843, 21, 41, "Print",ExpressionUUID->"13824424-fc4a-476a-b539-262092524089"],
Cell[12243, 328, 845, 21, 41, "Print",ExpressionUUID->"5441355a-c167-437a-a17c-cba5b63e37cf"],
Cell[13091, 351, 869, 22, 41, "Print",ExpressionUUID->"cc198d08-8e47-4b1d-bbae-1f86ac35b93a"],
Cell[13963, 375, 843, 21, 41, "Print",ExpressionUUID->"5656aa26-54d2-4702-b40d-fa67c3b9fed2"],
Cell[14809, 398, 843, 21, 41, "Print",ExpressionUUID->"831e4bfe-f50f-40d9-8ac1-a1ec173d38fa"],
Cell[15655, 421, 845, 21, 41, "Print",ExpressionUUID->"7b9ffd51-4b73-4a42-a1b8-3271872a9aed"],
Cell[16503, 444, 871, 22, 38, "Print",ExpressionUUID->"2f7b42c7-8c8d-44f2-aa4d-6980bbf187ba"],
Cell[17377, 468, 637, 15, 22, "Print",ExpressionUUID->"611811b1-a177-4f2e-b432-7c17a43fe6bc"],
Cell[18017, 485, 845, 21, 41, "Print",ExpressionUUID->"c4311994-5562-44d6-9041-2d99199c7dc2"],
Cell[18865, 508, 637, 15, 22, "Print",ExpressionUUID->"e71c5d03-8774-46fc-a0cd-e150a2d8361d"],
Cell[19505, 525, 974, 24, 38, "Print",ExpressionUUID->"eecae791-2287-41e7-be4b-6dade7e0816d"],
Cell[20482, 551, 747, 18, 22, "Print",ExpressionUUID->"88c96198-8c53-4567-acba-5903764eab6b"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

